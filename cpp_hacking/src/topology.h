/*
 * Copyright 2022 Max Planck Institute for Software Systems
 */

/*
Processes an MPI trace dataset and produces a topology
file used by hindsight-grpc
*/

#pragma once
#ifndef SRC_TOPOLOGY_H_
#define SRC_TOPOLOGY_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include "alibaba.h"
#include "mpi.h"
#include "../external/json.hpp"

namespace topology {

// Utility function to read a file
inline std::vector<std::string> readlines(std::string filename) {
  std::ifstream file(filename);

  std::vector<std::string> lines;
  std::string line;
  while(std::getline(file, line)) {
    lines.push_back(line);
  }

  return lines;
}

/*
To generate topology, we want to calculate per-service exec and transition probabilities
*/
struct InterfaceStats {

  InterfaceStats() {
    for (int i = 0; i < 8; i++) {
      transition_counts[i] = std::map<std::pair<std::pair<alibaba::Service*, alibaba::Interface*>, int>, int>();
    }
  }

  void AddSpan(mpi::Span* span) {
    // Track the number of calls
    callcount++;

    // Track execution time, subtracting child execution time.
    int exec = span->rt;
    int max_child_rt = 0;
    for (auto &child : span->children) {
      if (child->parent_rt > max_child_rt) {
        max_child_rt = child->parent_rt;
      }
    }
    exec -= max_child_rt;
    if (exec < 0) exec = 0;
    execs.push_back(exec);

    // Track transition probabilities, based on both child service, child interface, and rpctype
    for (int i = 0; i < 8; i++) {
      RPCType rpctype = (RPCType) i;
      std::map<std::pair<alibaba::Service*, alibaba::Interface*>, int> childcalls;
      for (auto &child : span->children) {
        if (child->rpctype == rpctype) {
          childcalls[{child->service, child->interface}]++;
        }
      }

      for (auto &p : childcalls) {
        auto &key = p.first;
        int count = p.second;

        for (int j = 0; j < count; j++) {
          transition_counts[i][{key, j}]++;
        }
      }
    }
  }

  float Exec() {
    int sum_exec = 0;
    for (auto &exec : execs) {
      sum_exec += exec;
    }
    if (execs.size() == 0) {
      return 0;
    } else {
      return ((float) sum_exec) / ((float) execs.size());
    }
  }

  int callcount = 0;
  std::vector<int> execs;
  std::map<std::pair<std::pair<alibaba::Service*, alibaba::Interface*>, int>, int> transition_counts[8];
};

class ServiceInfo {
public:

  ServiceInfo() {
    for (int i = 0; i < 8; i++) {
      callcount[i] = 0;
      interface_stats[i] = std::map<alibaba::Interface*, InterfaceStats*>();
    }
  }

  void AddSpan(mpi::Span* span) {
    std::map<alibaba::Interface*, InterfaceStats*> &s = interface_stats[(int) span->rpctype];

    auto it = s.find(span->interface);
    InterfaceStats* stats;
    if (it == s.end()) {
      stats = new InterfaceStats();
      s[span->interface] = stats;
    } else {
      stats = it->second;
    }
    stats->AddSpan(span);
    callcount[(int) span->rpctype]++;
  }

  std::string name;
  alibaba::Service* service;

  // Track stats per rpctype and per interface
  int callcount[8];
  
  std::map<alibaba::Interface*, InterfaceStats*> interface_stats[8];


};

class Generator {
public:

  Generator(mpi::Dataset* dataset) : dataset(dataset), services(dataset->service_count) {
    // Populate the service info
    for (auto &p : dataset->services) {
      alibaba::Service* service = p.second;
      services[service->ix].service = service;
    }

    calculateStats();
    generateServiceNames();
    generateInterfaceNames();
  }

  /* Gives services human-readable names by randomly assigning colors and animals */
  void generateServiceNames() {
    std::vector<std::string> colors = readlines("../data/colors.txt");
    std::vector<std::string> animals = readlines("../data/animals.txt");

    std::cout << "Assigning service names using " << colors.size() << " colors and " << animals.size() << " animals" << std::endl;

    if (colors.size() * animals.size() < dataset->service_count) {
      std::cout << "Not enough colors and animals to go round" << std::endl;
      return;
    }

    std::set<std::string> seen;
    for (auto &p : dataset->services) {
      alibaba::Service* service = p.second;

      while (true) {
        int cid = rand() % colors.size();
        int aid = rand() % animals.size();

        std::string name = colors[cid] + animals[aid];

        if (seen.find(name) != seen.end()) {
          continue;
        }

        seen.insert(name);
        services[service->ix].name = name;
        break;
      }
    }

    std::cout << "Assigned " << seen.size() << " names to " << services.size() << " services" << std::endl;
  }

  void generateInterfaceNames() {
    std::vector<std::string> nouns = readlines("../data/nouns.txt");

    for (ServiceInfo &service : services) {
      int interface_count = 0;
      for (int i = 0; i < rpctypes.size(); i++) {
        for (auto &pi : service.interface_stats[i]) {
          std::string rpctype = GetRPCTypeString((RPCType) i);
          alibaba::Interface* interface = pi.first;
          InterfaceStats* stats = pi.second;

          std::stringstream name;
          if (!pi.first->valid) {
            name << "default-" << rpctype;
          } else {
            name << nouns[rand() % nouns.size()] << "-" << rpctype << "-" << interface_count++;
          }
          api_names[{service.service, interface}] = name.str();
        }
      }
    }
  }

  void calculateStats() {
    for (auto &p : dataset->traces) {
      std::queue<mpi::Span*> to_visit;
      for (auto &span : p.second->roots) {
        to_visit.push(span);
      }
      for (auto &span : p.second->orphans) {
        to_visit.push(span);
      }

      while (!to_visit.empty()) {
        mpi::Span* next = to_visit.front();
        to_visit.pop();

        if (next->service->valid) {
          services[next->service->ix].AddSpan(next);
        }

        for (auto &span : next->children) {
          to_visit.push(span);
        }
      }
    }
  }
  
  void writeTopology(std::string filename, int mincalls) {

    auto out = std::ofstream(filename);

    // Now generate json
    nlohmann::json json;

    int rpctype = (int) RPCType::kRpc;

    int count = 0;

    std::vector<nlohmann::json> services_json;
    for (ServiceInfo &serviceinfo : services) {
      alibaba::Service* service = serviceinfo.service;
      if (!service->valid) continue;
      if (serviceinfo.callcount[rpctype] < mincalls) continue;
      count++;

      nlohmann::json service_json;
      service_json["name"] = serviceinfo.name;
      service_json["alibaba_id"] = service->id;

      std::vector<nlohmann::json> apis;
      for (auto &pi : serviceinfo.interface_stats[rpctype]) {
        alibaba::Interface* interface = pi.first;
        InterfaceStats* stats = pi.second;

        nlohmann::json api_json;
        std::vector<nlohmann::json> children_json;

        api_json["name"] = api_names[{service, interface}];
        api_json["alibaba_id"] = interface->id;
        api_json["exec"] = stats->Exec();
        api_json["alibaba_calls"] = stats->callcount;

        for (auto &pt : stats->transition_counts[rpctype]) {
          alibaba::Service* target_service = pt.first.first.first;
          alibaba::Interface* target_interface = pt.first.first.second;
          if (api_names.find(pt.first.first) == api_names.end()) continue;

          int prob = (int) (100 * ((float) pt.second) / ((float) stats->callcount));

          if (prob > 0) {
            nlohmann::json call_json;
            call_json["service"] = services[target_service->ix].name;
            call_json["api"] = api_names[pt.first.first];
            call_json["probability"] = prob;

            children_json.push_back(call_json);
          }
        }

        api_json["children"] = children_json;
        apis.push_back(api_json);
      }
      service_json["apis"] = apis;
      services_json.push_back(service_json);
    }

    json["services"] = services_json;

    out << json.dump(2) << std::endl;

    std::cout << "Processed " << count << " services" << std::endl;

    out.close();
  }

  mpi::Dataset* dataset;
  std::vector<ServiceInfo> services;
  std::map<std::pair<alibaba::Service*, alibaba::Interface*>, std::string> api_names;

};


}

#endif // SRC_TOPOLOGY_H_