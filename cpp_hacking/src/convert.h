/*
 * Copyright 2022 Max Planck Institute for Software Systems
 */

/*
This file has logic for converting between Alibaba's raw
trace representation (defined in alibaba.h) to our 
sanitized trace representation (defined in mpi.h)
*/

#pragma once
#ifndef SRC_CONVERT_H_
#define SRC_CONVERT_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include "alibaba.h"
#include "mpi.h"
#include "../external/json.hpp"

namespace convert {

// Convenience struct to keep track of bad calls during trace conversion
struct UnmatchedCall {
  UnmatchedCall(alibaba::Span* span, std::vector<mpi::Span*> &pars, mpi::Span* unmatched) : span(span), possible_parents(pars), unmatched(unmatched) {}
  alibaba::Span* span;
  std::vector<mpi::Span*> possible_parents;
  mpi::Span* unmatched;
};

// Stats for when we try to match calls during trace construction
struct ConversionStats {
  int strict_matches = 0;
  int regular_matches = 0;
  int unknown_matches = 0;
  int mq_matches = 0;
  int ancestor_matches = 0;
  int sibling_matches = 0;
  int orphans = 0;
  std::vector<int> call_counts;

  int alibaba_span_count = 0;
  int mpi_span_count = 0;
  int call_count = 0;
  int trace_count = 0;
  int root_count = 0;
  int compromised_trace_count = 0;


  int max_unmatched = 0;
  std::vector<UnmatchedCall*> unmatched;

};

/* Determines if the two calls match as caller-side/callee side pairs.
Returns -1 if this is the parent; 0 if they dont match; 1 if this is the child */
inline int match(alibaba::Call* a, alibaba::Call* b) {
  if (a->rpctype == RPCType::kMc) return 0; // MC calls are never matched
  bool ums_match = a->um == b->um || !a->um->valid || !b->um->valid;
  if (!ums_match) return 0;
  bool dms_match = a->dm == b->dm || !a->dm->valid || !b->dm->valid;
  if (!dms_match) return 0;
  bool ifaces_match = a->interface == b->interface || !a->interface->valid || !b->interface->valid;
  if (!ifaces_match) return 0;
  bool rpctypes_match = a->rpctype == b->rpctype || a->rpctype == RPCType::kEmpty || b->rpctype == RPCType::kEmpty;
  if (!rpctypes_match) return 0;

  // TODO(jcmace):Could try aligning timestamps too but skip for now
  //   We would also reach this point for a call with no details e.g. (?),rpc,(?) for which
  //    again we could try to match timestamps or something

  if (a->rt <= 0 && b->rt >= 0) {
    // This call is the callee; the other call is the caller
    return 1;
  }

  if (a->rt >= 0 && b->rt <= 0) {
    // This call is the caller; the other call is the callee
    return -1;
  }

  return 0;
}


/* Constructs zero or more MPI spans from an alibaba span. */
inline std::vector<mpi::Span*> MatchCalls(mpi::Dataset* dataset, mpi::Trace* trace, alibaba::Span* source) {
  std::vector<alibaba::Call*> &calls = source->calls;
  std::vector<mpi::Span*> new_spans;
  std::vector<bool> is_matched(source->calls.size());

  /*
  Often, the source span has zero or 1 calls, in which case
  we can trivially create a single MPI span.

  When there are 2 calls, they usually correspond to one call
  recorded by the caller, and one call recorded by the callee

  Occasionally there are more than 3 calls, which are bugs in
  the dataset, but we still need to decide what to do.
  */
  for (int i = 0; i < calls.size(); i++) {
    for (int j = 1; j < calls.size(); j++) {
      if (!is_matched[i] && !is_matched[j]) {
        int calls_match = match(calls[i], calls[j]);
        if (calls_match != 0) {
          mpi::Span* matched;
          if (calls_match < 0) {
            matched = new mpi::Span(trace, source->id, dataset->span_count++, calls[i], calls[j]);
          } else {
            matched = new mpi::Span(trace, source->id, dataset->span_count++, calls[j], calls[i]);
          }
          is_matched[i] = true;
          is_matched[j] = true;
          new_spans.push_back(matched);
        }
      }
    }
  }

  for (int i = 0; i < calls.size(); i++) {
    if (!is_matched[i]) {
      mpi::Span* span = new mpi::Span(trace, source->id, dataset->span_count++, calls[i]);
      new_spans.push_back(span);
    }
  }
  return new_spans;
}

/*
This is the main workhorse of trace conversion.

Starting from a root span of a trace, recursively traverses the child spans, 
creating mpi::Span instances, and fixing up any data problems
*/
inline void RecursivelyConvertSpans(mpi::Dataset* dataset, mpi::Trace* trace, alibaba::Span* source, std::vector<mpi::Span*> &possible_parents, std::vector<mpi::Span*> &roots, ConversionStats &stats) {
  std::vector<alibaba::Call*> &calls = source->calls;
  std::vector<mpi::Span*> spans = MatchCalls(dataset, trace, source);
  std::vector<bool> matched(spans.size());

  stats.call_count += calls.size();
  stats.alibaba_span_count++;
  stats.mpi_span_count += spans.size();

  if (stats.call_counts.size() < calls.size()+1) {
    stats.call_counts.resize(calls.size()+1, 0);
  }
  stats.call_counts[calls.size()]++;

  // First attempt a strict match on um/dm AND timestamps
  for (int i = 0; i < spans.size(); i++) {
    auto &span = spans[i];
    for (auto &parent : possible_parents) {
      if (!parent->service->valid) continue;
      if (span->parent_service != parent->service) continue;
      if (span->parent_timestamp < parent->timestamp) continue;
      if (span->parent_timestamp + span->parent_rt > parent->timestamp + parent->rt) continue;
      matched[i] = true;
      span->SetParent(parent);
      stats.strict_matches++;
      break;
    }
  }

  // Second attempt match only on um/dm
  for (int i = 0; i < spans.size(); i++) {
    if (matched[i]) continue;
    auto &span = spans[i];
    for (auto &parent : possible_parents) {
      if (!parent->service->valid) continue;
      if (span->parent_service != parent->service) continue;
      matched[i] = true;
      span->SetParent(parent);
      stats.regular_matches++;
      break;
    }
  }

  // Third attempt allows matches where parent or child has unknown service
  //  and instead we can infer based on rpcid
  for (int i = 0; i < spans.size(); i++) {
    if (matched[i]) continue;
    auto &span = spans[i];
    for (auto &parent : possible_parents) {
      if (span->parent_service->valid && parent->service->valid) continue;
      matched[i] = true;
      span->SetParent(parent);
      stats.unknown_matches++;
      break;
    }
  }

  // MQ calls often record a different UM on caller and callee side
  for (int i = 0; i < spans.size(); i++) {
    if (matched[i]) continue;
    auto &span = spans[i];
    for (auto &parent : possible_parents) {
      if (span->rpctype == RPCType::kMq && parent->rpctype == RPCType::kMq) {
        matched[i] = true;
        span->SetParent(parent);
        stats.mq_matches++;
        break;
      }
    }
  }

  // MC calls often accidentally advance rpcid when parent-child doesnt exist
  // And some RPC calls too
  for (int i = 0; i < spans.size(); i++) {
    if (matched[i]) continue;
    auto &span = spans[i];
    if (span->calls.size() == 0) continue;
    alibaba::Call* child_call = span->calls[0];
    std::queue<mpi::Span*> to_visit;
    for (auto &parent : possible_parents) {
      to_visit.push(parent);
    }
    while (!to_visit.empty()) {
      mpi::Span* parent = to_visit.front();
      to_visit.pop();

      if (parent->parent != nullptr) {
        to_visit.push(parent->parent);
      }

      if (parent->calls.size() == 0) continue;
      alibaba::Call* parent_call = parent->calls[0];

      if (parent_call->um != child_call->um) continue;

      // This should be a sibling of this parent
      if (parent->parent != nullptr) {
        span->SetParent(parent->parent);
      } else {
        roots.push_back(span);
      }
      matched[i] = true;

      stats.ancestor_matches++;
      break;
    }
  }

  // Lastly deal with orphans, which can occur if the rpcid isn't properly extended
  // on a child call.
  // An orphan call is *potentially* actually a child of one of our children
  for (int i = 0; i < spans.size(); i++) {
    if (matched[i]) continue;
    auto &span = spans[i];
    for (int j = i; j < spans.size(); j++) {
      if (!matched[j]) continue;
      auto &sibling = spans[j];

      if (sibling->service != span->parent_service) continue;
      matched[i] = true;
      span->SetParent(sibling);
      stats.sibling_matches++;
      break;
    }
  }


  // Now we do our own child spans
  for (auto &child : source->children) {
    if (spans.size() > 0) {
      RecursivelyConvertSpans(dataset, trace, child, spans, roots, stats);
    } else {
      // This span has no calls, so just jump down a level
      RecursivelyConvertSpans(dataset, trace, child, possible_parents, roots, stats);
    }
  }

  // For now we return all orphans
  bool has_unmatched = false;
  for (int i = 0; i < spans.size(); i++) {
    if (!matched[i]) {
      roots.push_back(spans[i]);
      stats.orphans++;

      if (stats.unmatched.size() < stats.max_unmatched) {
        stats.unmatched.push_back(new UnmatchedCall(source, possible_parents, spans[i]));
      }
    }
  }
}

inline mpi::Trace* ConvertTrace(mpi::Dataset* dataset, alibaba::Trace* source, ConversionStats &stats) {

  mpi::Trace* trace = dataset->GetTrace(source);
  std::vector<mpi::Span*> possible_parents;
  for (auto &root : source->roots) {
    RecursivelyConvertSpans(dataset, trace, root, possible_parents, trace->orphans, stats);
    trace->roots.push_back(trace->orphans.back());
    trace->orphans.pop_back();
  }
  stats.root_count += source->roots.size();

  if (trace->orphans.size() > 0) {
    stats.compromised_trace_count++;
  } else {
    stats.trace_count++;
  }

  return trace;
}

inline void printConversionStats(ConversionStats &stats) {
  std::cout << "Valid Traces:     " << stats.trace_count << std::endl;
  std::cout << "Compromised:      " << stats.compromised_trace_count << std::endl;
  std::cout << "Roots:            " << stats.root_count << std::endl;
  std::cout << "Strict matches:   " << stats.strict_matches << std::endl;
  std::cout << "Regular matches:  " << stats.regular_matches << std::endl;
  std::cout << "Unknown matches:  " << stats.unknown_matches << std::endl;
  std::cout << "Ancestor matches: " << stats.ancestor_matches << std::endl;
  std::cout << "Sibling matches:  " << stats.sibling_matches << std::endl;
  std::cout << "Orphans:          " << stats.orphans << std::endl;
}

inline mpi::Dataset* ConvertDataset(alibaba::Dataset* source) {
  mpi::Dataset* dataset = new mpi::Dataset(source);

  ConversionStats stats;

  for (auto &p : source->traces) {
    ConvertTrace(dataset, p.second, stats);
  }

  printConversionStats(stats);

  return dataset;
}


}

#endif // SRC_CONVERT_H_