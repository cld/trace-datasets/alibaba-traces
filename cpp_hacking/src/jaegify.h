/*
 * Copyright 2022 Max Planck Institute for Software Systems
 */

/*
TODO: Describe file
*/

#pragma once
#ifndef SRC_JAEGIFY_H_
#define SRC_JAEGIFY_H_

#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include "alibaba.h"
#include "mpi.h"
// #include "topology.h"
#include "../external/json.hpp"

namespace jaegify {

  class Tag{
    public:

    Tag(std::string key, std::string type, std::string value) : key(key), type(type), value(value) {} 

    
    const std::string key;
    const std::string type;
    const std::string value;
  };

  class Reference{
    public:

    Reference(std::string refType, std::string traceID, uint32_t spanID) : refType(refType), traceID(traceID), spanID(spanID) {} 

    
    const std::string refType;
    const std::string traceID;
    const uint32_t spanID;
  };

class jaegerSpan{
  public:

  jaegerSpan(std::string tID, uint32_t parentID, mpi::Span* mpiSpan,std::set<uint32_t> &process_ids){
    traceID=tID;
    spanID=mpiSpan->ix;
    flags=0;
    operationName=mpiSpan->interface->id;
    if(parentID>0){
      references.push_back(Reference("CHILD_OF",tID,parentID));
    }
    
    startTime=mpiSpan->parent_timestamp;
    duration=abs(mpiSpan->parent_rt);

    tags.push_back(Tag("root.timestamp","timestamp",std::to_string(mpiSpan->timestamp)));
    tags.push_back(Tag("root.duration","time",std::to_string(abs(mpiSpan->rt))));
    
    processID=std::to_string(mpiSpan->service->ix);

    process_ids.insert(mpiSpan->service->ix);

  }

  void print(){
    std::cout<<"traceID: "<<traceID<<std::endl;
    std::cout<<"spanID: "<<spanID<<std::endl;
    std::cout<<"flags: "<<flags<<std::endl;
    std::cout<<"operationName: "<<operationName<<std::endl;
    std::cout<<"references: "<<std::endl;
    for(auto &ref : references){
      std::cout<<"\t"<<"refType: "<<ref.refType<<std::endl;
      std::cout<<"\t"<<"traceID: "<<ref.traceID<<std::endl;
      std::cout<<"\t"<<"spanID: "<<ref.spanID<<std::endl;
    }
    std::cout<<"startTime: "<<startTime<<std::endl;
    std::cout<<"duration: "<<duration<<std::endl;
    std::cout<<"tags: "<<std::endl;
    for(auto &tag : tags){
      std::cout<<"\t"<<"key: "<<tag.key<<std::endl;
      std::cout<<"\t"<<"type: "<<tag.type<<std::endl;
      std::cout<<"\t"<<"value: "<<tag.value<<std::endl;
    }
    std::cout<<"processID: "<<processID<<std::endl;

  }

  std::string traceID;
  uint32_t spanID;
  int flags;
  std::string operationName;
  std::vector<Reference> references;
  uint32_t startTime;
  int16_t duration;
  std::vector<Tag> tags;
  //logs;
  std::string processID;
  std::string warnings;
};

class Process{
  public:
  Process(topology::ServiceInfo &serviceInfo) {
    processID = serviceInfo.service->ix;
    serviceName = serviceInfo.name;

    tags.push_back(Tag("hostname","string",serviceName));

    sI = serviceInfo;

  } 

  void print(){
    std::cout<<"Process id: "<<processID<<" serviceName: "<<serviceName<<std::endl;
  }

  uint32_t processID;
  std::string serviceName;
  std::vector<Tag> tags;
  topology::ServiceInfo sI;
};

class jaegerTrace{
  public:

  jaegerTrace(mpi::Trace* mpiTrace, std::vector<topology::ServiceInfo> &services){
    std::set<uint32_t> process_ids; 
    
    traceID=mpiTrace->id;
    getSpans(mpiTrace,spans,process_ids);
    
    populateProcesses(process_ids, services);
   
  }

  void summaryPrint(){
    std::cout<<"Trace ID: "<<traceID<<std::endl;
    std::cout<<"Number of Spans: "<<spans.size()<<std::endl;
    for(int i = 0; i<spans.size(); i++){
      std::cout<<"#";
    }
    std::cout<<std::endl;
    std::cout<<"Number of Processes: "<<processes.size()<<std::endl<<std::endl;
  }

  void print(){
    std::cout<<"Trace ID: "<<traceID<<std::endl<<std::endl;
    std::cout<<"Spans: "<<std::endl<<std::endl;

    for(auto &span : spans){
      span.print();
    }

    std::cout<<std::endl<<"Processes: "<<std::endl<<std::endl;
    for(auto &process : processes){
      process.print();
    }
  }

  void getSpanRecursive(int level, std::string traceID, uint32_t parentID, std::vector<jaegerSpan> &spanList, std::set<uint32_t> &process_ids, mpi::Span* span){

  jaegerSpan JS = jaegerSpan(traceID, parentID, span, process_ids);
  spanList.push_back(JS);

  if(span->children.size()>0){
    // std::cout << indent <<"children("<<span->children.size()<<"): "<<std::endl;
    level++;
    for(auto &child : span->children) {
      getSpanRecursive(level,traceID,span->ix,spanList,process_ids,child);
    }

  }
  }

  void getSpans(mpi::Trace* trace, std::vector<jaegerSpan> &spanList, std::set<uint32_t> &process_ids){
    for(auto &span : trace->roots){
      getSpanRecursive(0,trace->id,0,spanList,process_ids,span);
    
    }
    // std::map<std::string,std::string> cM;
    // cM["test"]="test1";
    
  }

  void populateProcesses(std::set<uint32_t> &process_ids, std::vector<topology::ServiceInfo> &services){
    for (auto &p:process_ids){
      processes.push_back(Process(services[p]));
      // std::cout<<p<<std::endl;
      // std::cout<<services[p].service->ix<<": "<<services[p].name<<std::endl;
    }
  }

  std::string traceID;
  std::vector<jaegerSpan> spans;
  std::vector<Process> processes;
  std::string warnings;
  int total;
  int limit;
  int offset;
  std::string errors;
};

class Converter {
public:

  Converter(mpi::Dataset* dataset) : dataset(dataset), services(dataset->service_count) {
    // Populate the service info
    for (auto &p : dataset->services) {
      alibaba::Service* service = p.second;
      services[service->ix].service = service;
    }

    generateServiceNames();

  }

  /* Gives services human-readable names by randomly assigning colors and animals */
  void generateServiceNames() {
    std::vector<std::string> colors = topology::readlines("../data/colors.txt");
    std::vector<std::string> animals = topology::readlines("../data/animals.txt");

    std::cout << "Assigning service names using " << colors.size() << " colors and " << animals.size() << " animals" << std::endl;

    if (colors.size() * animals.size() < dataset->service_count) {
      std::cout << "Not enough colors and animals to go round" << std::endl;
      return;
    }

    std::set<std::string> seen;
    for (auto &p : dataset->services) {
      alibaba::Service* service = p.second;

      while (true) {
        int cid = rand() % colors.size();
        int aid = rand() % animals.size();

        std::string name = colors[cid] + animals[aid];

        if (seen.find(name) != seen.end()) {
          continue;
        }

        seen.insert(name);
        services[service->ix].name = name;
        break;
      }
    }

    std::cout << "Assigned " << seen.size() << " names to " << services.size() << " services" << std::endl;
  }

  void populateJaegerTraces(){

    int cnt = 0;

    for(auto &tracePair : dataset->traces){
          if(tracePair.second->orphans.size()==0){
            cnt++;
            jaegerTrace JT = jaegerTrace(tracePair.second,services);
            
            jaegerTraces.push_back(JT);


          }
      }


  }

  void printJaegerTraces(std::string opt){
    for(auto &JT : jaegerTraces){
      if(opt=="full"){
        JT.print();
      }else{
        JT.summaryPrint();
      }
    }
  }

  void printSpan(int level, mpi::Span* span){

  std::string indent = "";

  for(int i=0; i<level; i++){
    indent+="\t";
  }

  std::cout << indent <<"spanID: "<<span->ix<<std::endl;
  std::cout << indent <<"timestamp: "<<span->timestamp<<std::endl;
  std::cout << indent <<"duration: "<<span->rt<<std::endl;
  std::cout << indent <<"service: "<<span->service->ix<<std::endl<<std::endl;

  if(span->children.size()>0){
    std::cout << indent <<"children("<<span->children.size()<<"): "<<std::endl;
    level++;
    for(auto &child : span->children) {
      printSpan(level,child);
    }

  }

}

  void printTrace(mpi::Trace* trace){
    std::cout << "traceID: " << trace->id << std::endl;
      

    printSpan(0,trace->roots.front());

    std::cout << std::endl;
  }

  void printValidTraces(){
      std::cout << "Printing valid traces" << std::endl;

        int cnt = 0;

      for(auto &tracePair : dataset->traces){
          if(tracePair.second->orphans.size()==0){

          std::cout<<cnt<<": "<<tracePair.first<<std::endl;
          printTrace(tracePair.second);
          cnt++;

          }
      }
  }

  std::string setFileName(std::string namingConvention, jaegerTrace JT, int count){

    switch(formatMap[namingConvention]){
      case 1:
        return "Trace-"+std::to_string(count);
        break;
      case 2:
        return("Trace-"+std::to_string(count)+"_Spans-"+std::to_string(JT.spans.size()));
        break;
      case 3:
        return(JT.traceID);
        break;
    }
    
    return JT.traceID;
  }

  std::string setBulkFileName(std::string namingConvention, std::string timestamp, int count){

    switch(formatMap[namingConvention]){
      case 1:
        return "Batch-"+timestamp;
        break;
      case 2:
        return "Batch-"+timestamp+"_Traces-"+std::to_string(count);
        break;
      case 3:
        return "Batch-"+timestamp;
        break;
    }
    
    return "Batch";
  }

  void writeJsonToFile(nlohmann::json trace_json, std::string filename, std::string location){
    
    auto out = std::ofstream(location+filename+".json");

    nlohmann::json json;

    std::vector<nlohmann::json> data_json;

    data_json.push_back(trace_json);

    json["data"]=data_json;

    json["total"]=0;
    json["limit"]=0;
    json["offset"]=0;
    json["errors"]={};

    // std::cout<<json.dump(2)<<std::endl;
    out << json.dump(2) <<std::endl;

    out.close();

  }

  void writeBulkJsonToFile(nlohmann::json data_json, std::string filename, std::string location){

    auto out = std::ofstream(location+filename+".json");

    nlohmann::json json;

    json["data"]=data_json;

    json["total"]=0;
    json["limit"]=0;
    json["offset"]=0;
    json["errors"]={};

    // std::cout<<json.dump(2)<<std::endl;
    out << json.dump(2) <<std::endl;

    out.close();


  }

  nlohmann::json getTraceJson(jaegerTrace JT){
    

    nlohmann::json trace_json;

    trace_json["traceID"] = JT.traceID;

    std::vector<nlohmann::json> spans_json;

    for(auto &s : JT.spans){
      nlohmann::json span_json;
      span_json["traceID"] = JT.traceID;
      span_json["spanID"] = s.spanID;
      span_json["flags"] = 0;
      span_json["operationName"] = s.operationName;

      std::vector<nlohmann::json> references_json;

      for(auto &ref : s.references){
        nlohmann::json reference_json;

        reference_json["refType"]=ref.refType;
        reference_json["traceID"]=ref.traceID;
        reference_json["spanID"]=ref.spanID;
        
        references_json.push_back(reference_json);
      }

      span_json["references"] = references_json;
      span_json["startTime"] = s.startTime;
      span_json["duration"] = s.duration;
      
      std::vector<nlohmann::json> tags_json;

      for(auto &tag : s.tags){
        nlohmann::json tag_json;

        tag_json["key"] = tag.key;
        tag_json["type"] = tag.type;
        tag_json["value"] = tag.value;
        
        tags_json.push_back(tag_json);
      }
      span_json["tags"] = tags_json;

      std::vector<nlohmann::json> logs_json;

      // for(auto &ref : s.logs){
      //   nlohmann::json log_json = {};
        
      //   logs_json.push_back(log_json);
      // }
      
      span_json["logs"] = logs_json;
      
      
      
      span_json["processID"]="p"+s.processID;
      span_json["warnings"]={};

      spans_json.push_back(span_json);
    }

    trace_json["spans"] = spans_json;

    nlohmann::json processes_json;

    for(auto &proc : JT.processes){
      nlohmann::json process_json;
      process_json["serviceName"] = proc.serviceName;

      
      
      std::vector<nlohmann::json> tags_json;

      for(auto &tag : proc.tags){
        nlohmann::json tag_json;

        tag_json["key"] = tag.key;
        tag_json["type"] = tag.type;
        tag_json["value"] = tag.value;
        
        tags_json.push_back(tag_json);
      }
      process_json["tags"] = tags_json;

      

      std::string procID = "p"+std::to_string(proc.processID);
      processes_json[procID] = process_json;

    }

    trace_json["processes"] = processes_json;

    trace_json["warnings"] = {};
    

    return trace_json;
  }
  
  void writeTraces(bool singleFile, std::string namingConvention, std::string location = "jaeger-output"){
    std::cout<<std::endl<<"Attempting to write traces to file"<<std::endl;
    std::string timestampString,simpletimestampString;
    std::string loc;
    int numTraces = jaegerTraces.size();

    auto t = std::time(nullptr);
      auto tm = *std::localtime(&t);

      std::ostringstream oss;
      oss << std::put_time(&tm, "%d-%m-%Y_%H:%M:%S");
      timestampString = oss.str();
      oss.str("");
      oss << std::put_time(&tm, "%d%m%Y_%H%M");
      simpletimestampString = oss.str();
    if (location=="jaeger-output"){
      

      loc = "../jaeger-output/"+timestampString;
    }else{
      loc = "../jaeger-output/"+location;
    }
    std::string mkdir = "mkdir "+loc;
    
    std::cout<<"Executing: "<<mkdir<<std::endl<<std::endl;
 
    std::system(&mkdir[0]);


    std::vector<nlohmann::json> data_json;
    int cnt=0;
    double progress = 0.0;
    std::cout<<"Writing "<<numTraces<<" traces to file"<<std::endl;
    for(auto &JT: jaegerTraces){
      if(cnt%50==0){
        // std::cout<<cnt<<std::endl;
         progress = cnt/(double)numTraces;
       printProgress(progress);
      }
      cnt++;
      
      
      nlohmann::json traceJson = getTraceJson(JT);
      
      if(singleFile){
        data_json.push_back(traceJson);
      }else{
        std::string filename = setFileName(namingConvention,JT,cnt);
        writeJsonToFile(traceJson,filename,loc+"/");
      }


    }
    printProgress(1.0);
    std::cout<<std::endl;

    if(data_json.size()>0){
      std::string filename = setBulkFileName(namingConvention,simpletimestampString,numTraces);
      writeBulkJsonToFile(data_json,filename,loc+"/");
    }



    std::cout<<"Successfully wrote "<<numTraces<<" traces to file"<<std::endl<<std::endl;

  }


void printProgress(double progress) {
    int barWidth = 70;

    std::cout << "[";
    int pos = barWidth * progress;
    for (int i = 0; i < barWidth; ++i) {
        if (i < pos) std::cout << "=";
        else if (i == pos) std::cout << ">";
        else std::cout << " ";
    }
    std::cout << "] " << int(progress * 100.0) << " %\r";
    std::cout.flush();
}



  mpi::Dataset* dataset;
  std::vector<mpi::Trace*> validTraces;
  std::vector<jaegerTrace> jaegerTraces;
  std::map<std::string, std::string> childMap;
  std::vector<topology::ServiceInfo> services;
  std::map<std::pair<alibaba::Service*, alibaba::Interface*>, std::string> api_names;

  std::map<std::string, int> formatMap = {{"super-simple",1},{"simple",2},{"id",3}};

};



}

#endif // SRC_JAEGIFY_H_