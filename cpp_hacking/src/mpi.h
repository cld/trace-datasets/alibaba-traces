/*
 * Copyright 2022 Max Planck Institute for Software Systems
 */

/*
This file defines a more processed version of the Alibaba traces that
deals with inconsistencies in calls etc.

*/

#pragma once
#ifndef SRC_MPI_H_
#define SRC_MPI_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include "alibaba.h"
#include "../external/json.hpp"

namespace mpi {

class Span;
class Trace final {
public:
  Trace(alibaba::Trace* source, uint32_t ix) : source(source), id(source->id), ix(ix) {}

  alibaba::Trace* source;
  const std::string id;
  const uint32_t ix;
  std::vector<Span*> roots;
  std::vector<Span*> orphans;
};

class Span final {
public:

  /* A span that we only measured once, at either caller or callee */
  Span(Trace* trace, int rpcid, uint32_t ix, alibaba::Call* call) : trace(trace), parent(nullptr), rpcid(rpcid), ix(ix) {
    calls = {call};
    parent_timestamp = call->timestamp;
    timestamp = call->timestamp;
    rt = call->rt;
    parent_rt = call->rt;
    rpctype = call->rpctype;
    service = call->dm;
    parent_service = call->um;
    interface = call->interface;
  }

  /* A sanitized representation of a span that corresponds to a specific call. */
  Span(Trace* trace, int rpcid, uint32_t ix, alibaba::Call* caller, alibaba::Call* callee) : trace(trace), parent(nullptr), rpcid(rpcid), ix(ix) {
    calls = {caller, callee};
    parent_timestamp = caller->timestamp;
    timestamp = callee->timestamp;
    rt = -callee->rt;
    parent_rt = caller->rt;
    rpctype = caller->rpctype;
    if (rpctype == RPCType::kEmpty) {
      rpctype = callee->rpctype;
    }
    service = callee->dm;
    if (!service->valid) {
      service = caller->dm;
    }
    parent_service = callee->um;
    if (!parent_service->valid) {
      parent_service = caller->um;
    }
    interface = callee->interface;
    if (!interface->valid) {
      interface = caller->interface;
    }
  }

  void SetParent(Span* parent) {
    this->parent = parent;

    if (parent->service->valid) {
      parent_service = parent->service;
    } else if (parent_service->valid) {
      parent->service = parent_service;
      for (auto &sibling : parent->children) {
        sibling->parent_service = parent_service;
      }
    }

    parent->children.push_back(this);
  }

  const int rpcid; // RPCId taken from the trace -- not necessarily unique due to data issues
  const uint32_t ix;
  Trace* trace;
  Span* parent; // null if root span
  std::vector<Span*> children;

  uint32_t parent_timestamp; // Timestamp at the callee service
  uint32_t timestamp; // Timestamp of the caller
  int16_t rt; // RT at the callee service
  int16_t parent_rt; // RT measured by the caller
  RPCType rpctype;
  
  alibaba::Service* parent_service;
  alibaba::Service* service; //serviceName
  alibaba::Interface* interface; //operationName

  std::vector<alibaba::Call*> calls;
};


class Dataset {
public:
  Dataset(alibaba::Dataset* alibaba) : alibaba(alibaba), services(alibaba->services), 
    interfaces(alibaba->interfaces), service_count(alibaba->service_count), 
    interface_count(alibaba->interface_count) {}


  alibaba::Service* GetService(std::string &id) {
    return alibaba->GetService(id);
  }

  alibaba::Interface* GetInterface(std::string &id) {
    return alibaba->GetInterface(id);
  }

  Trace* GetTrace(alibaba::Trace* source) {
    auto it = traces.find(source->id);
    if (it != traces.end()) {
      return it->second;
    }
    Trace* trace = new Trace(source, trace_count++);
    traces[trace->id] = trace;
    return trace;
  }

  alibaba::Dataset* alibaba;
  std::map<std::string, alibaba::Service*> &services;
  std::map<std::string, alibaba::Interface*> &interfaces;
  std::map<std::string, Trace*> traces;

  uint32_t call_count = 0;
  uint32_t span_count = 0;
  uint32_t service_count;
  uint32_t interface_count;
  uint32_t trace_count = 0;

};

}

#endif // SRC_MPI_H_