/*
 * Copyright 2022 Max Planck Institute for Software Systems
 */

/*
This file defines a representation of the raw Alibaba trace files, and methods
for constructing traces from these trace files.

We do not attempt to do any sanitization / correction of the trace data here.
This is primarily about constructing objects.

You can see data/example.csv for an example of the raw Alibaba traces.
*/

#pragma once
#ifndef SRC_ALIBABA_H_
#define SRC_ALIBABA_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include "../external/json.hpp"


// Values for the rpctype column
enum class RPCType : int16_t {
  kUnknown = 0, // An unknown rpc type
  kEmpty, // ""
  kRpc, // "rpc"
  kHttp, // "http"
  kDb, // "db"
  kMc, // "mc"
  kMq, // "mq"
  kUserDefined // "userDefined"
};
static std::unordered_map<std::string,RPCType> const rpctypes = {
  {"",RPCType::kEmpty}, {"rpc",RPCType::kRpc}, {"http",RPCType::kHttp},
  {"db",RPCType::kDb}, {"mc", RPCType::kMc}, {"mq", RPCType::kMq}, {"userDefined",RPCType::kUserDefined}};

inline RPCType GetRPCType(std::string &rpctype) {
  auto it = rpctypes.find(rpctype);
  if (it != rpctypes.end()) {
    return it->second;
  }
  return RPCType::kUnknown;
}

inline std::string GetRPCTypeString(RPCType rpctype) {
  for (auto &p : rpctypes) {
    if (p.second == rpctype) {
      return p.first;
    }
  }
  return "invalidRPCType";
}

namespace alibaba {

/* 
The `interface` column of the trace data can (optionally) specify an 
interface ID.

Sometimes the `interface` column could be blank or a question mark
ie. "" or "(?)"

Note that the same interface ID can appear for DIFFERENT dm IDs,
so there is not a direct correspondence from a Interface
to a specific Service.
*/
class Interface final {
 public:
  Interface(std::string &id, uint32_t ix) : id(id), ix(ix), valid(true) {}
  Interface(std::string &id, uint32_t ix, bool valid) : id(id), ix(ix), valid(valid) {}

  const std::string id;
  const uint32_t ix; // An integer ID that we assign, starting from 0, useful for faster indexing.
  const bool valid; // For "" and "(?)" IDs, this will be false.  For all others, it will be true.
};

/*
The `um` and `dm` columns of the trace data can (optionally) specify
a service ID.

The `um` and `dm` columns can sometimes be blank or a question mark
ie. "" or "(?)"
*/
class Service final {
 public:

  Service(std::string &id, uint32_t ix) : id(id), ix(ix), ums(), valid(true) {
    _initRpcTypes();
  }

  Service(std::string &id, uint32_t ix, bool valid) : id(id), ix(ix), ums(), valid(valid) {
    _initRpcTypes();
  }

  void _initRpcTypes() {
    for (int i = 0; i < 8; i++) {
      seen_rpctypes[i] = 0;
    }
  }

  void AddUm(Service* um) {
    ums.insert(um);
  }
  
  void SetType(RPCType rpctype) {
    seen_rpctypes[(int) rpctype] = 1;
  }

  bool Is(RPCType rpctype) {
    return seen_rpctypes[(int) rpctype];
  }

  void print() {
    std::cout << "Service " << id;
    for (auto &p : rpctypes) {
      if (seen_rpctypes[(int) p.second]) {
        std::cout << " " << p.first;
      }
    }
    std::cout << " " << ums.size() << " ums:" << std::endl;
    for (auto &um : ums) {
      std::cout << "   " << um->id << std::endl;
    }
  }

  const bool valid; // For "" and "(?)" IDs, this will be false.  For all others, it will be true.
  const std::string id; // The ID in the dataset, e.g. 5cca70246befb1f4c9546d2912b9419dee54439218efa55a7a2e0e26e86ad749
  const uint32_t ix; // An integer ID that we assign, starting from 0, useful for faster indexing.
  bool seen_rpctypes[8];
  std::set<Service*> ums;
};

/* 
A call corresponds to a single row of the trace file.
*/
class Call final {
 public:
  Call(uint32_t timestamp, RPCType rpctype, int16_t rt, Service* um, Service* dm, Interface* interface, uint32_t ix)
    : ix(ix), timestamp(timestamp), rpctype(rpctype), rt(rt), um(um), dm(dm), interface(interface) {}

  const uint32_t ix; // An integer ID that we assign, starting from 0, useful for faster indexing.
  const uint32_t timestamp; // in milliseconds  
  const RPCType rpctype;
  const int16_t rt;
  Service* um;
  Service* dm;
  Interface* interface;

  std::string str() {
    std::stringstream s;
    s << timestamp << "," << um->id << "," << GetRPCTypeString(rpctype) << "," << dm->id << "," << interface->id << "," << rt;
    return s.str();
  }
};

/* 
A span is a grouping of zero or more calls.

All of the calls in a span will exactly match on `traceid` and `rpcid`.

If a span exists for some rpcid (e.g. `1.2.6.10`) then a span will exist
for the parent prefix (ie. `1.2.6`).  The `parent` field of a span will point
to this parent span.

Thus it's possible a span might contain 0 calls if there were none in the dataset,
because we will create spans for all components of the rpcid.
For example, if we only had a single line in the trace file with rpcid `1.2.6.10`, 
we would create four spans corresponding to `1`, `1.2`, `1.2.6`, and `1.2.6.10`.
Only the span for `1.2.6.10` would actually have a call associated with it.

Some spans can have more than one calls associated with them, if there are more than
one lines in the trace file with the same traceid and rpcid.  This can happen
if both the caller and callee record the call.  It can also happen due to bugs in the
data or in the application's instrumentation.  Sometimes we encounter spans with
tens of calls.  These cases presumably happen when:
* The application makes a child call but forgets to extend the `rpcid` with a new component
* The application makes a sibling call but forgets to increment the final component of the rpcid
* A child call completes and returns to the parent call but continues using the child call's rpcid.

We don't attempt to handle any of the edge cases here.  The purpose of this class is simply
to accumulate all calls that match on `traceid` and `rpcid`.
*/
class Trace;
class Span final {
 public:
  Span(Trace* trace, int id_, uint32_t ix) : trace(trace), parent(nullptr), id(id_), ix(ix), children(0), calls(0) {}
  Span(Trace* trace, Span* parent, int id_, uint32_t ix) : trace(trace), parent(parent), id(id_), ix(ix), children(0), calls(0) {}

  Span* GetChildSpan(int childid, uint32_t &span_count) {
    for (int i = 0; i < children.size(); i++) {
      if (children[i]->id == childid) {
        return children[i];
      }
    }
    Span* child = new Span(trace, this, childid, span_count++);
    children.push_back(child);
    return child;
  }

  void AddCall(Call* call) {
    calls.push_back(call);
  }

  /* Useful for debugging / printing -- reconstruct the fully qualified ID string */
  std::string idstr() {
    std::string s = std::to_string(id);
    Span* prev = parent;
    while (prev != nullptr) {
      s = std::to_string(prev->id) + "." + s;
      prev = prev->parent;
    }
    return s;
  }

  const int id;
  const uint32_t ix; // An integer ID that we assign, starting from 0, useful for faster indexing.
  Trace* trace; // Never null
  Span* parent; // Can be null
  std::vector<Span*> children; // zero or more child spans
  std::vector<Call*> calls; // zero or more calls in a span
};

/* 
A trace is uniquely identified by an ID.

We collect and group all calls belonging to a trace into some number of spans.

A trace will have one or more roots
 */
class Trace final {
 public:
  Trace(std::string &id, uint32_t ix) : id(id), ix(ix), roots() {}

  Span* GetSpan(std::string &rpcid, uint32_t &span_count) {    
    std::stringstream s(rpcid);
    std::string idstr;
    
    // First ID is the root ID
    if (!std::getline(s, idstr, '.')) return nullptr;
    int id = (int) std::stoll(idstr);

    // Get the root span
    Span* next = nullptr;
    for (int i = 0; i < roots.size(); i++) {
      if (roots[i]->id == id) {
        next = roots[i];
        break;
      }
    }
    if (next == nullptr) {
      next = new Span(this, id, span_count++);
      roots.push_back(next);
    }

    // Iterate through the children
    while (std::getline(s, idstr, '.')) {
      id = (int) std::stoll(idstr);
      next = next->GetChildSpan(id, span_count);
    }
    
    return next;
  }

  const std::string id;
  const uint32_t ix; // An integer ID that we assign, starting from 0, useful for faster indexing.
  std::vector<Span*> roots;
};

/*
A collection of traces.
*/
class Dataset {
public:
  Dataset() {
    // Pre-populate the known invalid services
    std::string none = "none";
    Service* invalidService = new Service(none, service_count++, false);
    services[""] = invalidService;
    services["(?)"] = invalidService;

    Interface* invalidInterface = new Interface(none, interface_count++, false);
    interfaces[""] = invalidInterface;
    interfaces["(?)"] = invalidInterface;
  }

  void AddCall(
    std::string &trace_id,
    uint32_t &timestamp,
    std::string &rpcid,
    std::string &um_id,
    RPCType &rpctype,
    std::string &dm_id,
    std::string &interface_id,
    int16_t &rt
  ) {
    // Get or create the trace
    Trace* t = GetTrace(trace_id);

    // Get or create the span and all parent spans
    Span* span = t->GetSpan(rpcid, span_count);

    bool has_interface = interface_id != "" && interface_id != "(?)";
    bool has_dm = dm_id != "" && dm_id != "(?)";
    bool has_um = um_id != "" && um_id != "(?)";

    Service* um = GetService(um_id);
    Service* dm = GetService(dm_id);
    Interface* interface = GetInterface(interface_id);

    Call* call = new Call(timestamp, rpctype, rt, um, dm, interface, call_count++);
    span->AddCall(call);

    if (has_dm) {
      dm->SetType(rpctype);
    }

    if (has_um && has_dm) {
      dm->AddUm(um);
    }

    calls.push_back(call);
  }

  int AddLine(std::string &line) {
    std::stringstream linestream(line);
    std::string cell;
    
    // Ignore first cell
    if (!std::getline(linestream, cell, ',')) return 1;

    // Second cell is traceid
    std::string traceid;
    if (!std::getline(linestream, traceid, ',')) return 1;
    if (traceid == "") return 2; // Ignore lines with no traceId

    // Third cell is timestamp, Range from 0 to 43200000, increments of 30000
    if (!std::getline(linestream, cell, ',')) return 1;
    uint32_t timestamp = std::stoul(cell);

    // Fourth cell is RPCid e.g. "0.1.3.1.1.1.12"
    std::string rpcid;
    if (!std::getline(linestream, rpcid, ',')) return 1;
    if (rpcid == "") return 3; // Ignore lines with no rpcid

    // Fifth cell is UM RPC ID
    std::string um;
    if (!std::getline(linestream, um, ',')) return 1;

    // Sixth cell is RPC type
    if (!std::getline(linestream, cell, ',')) return 1;
    RPCType rpctype = GetRPCType(cell);

    // Fifth cell is DM RPC ID
    std::string dm;
    if (!std::getline(linestream, dm, ',')) return 1;

    // Sixth cell is optional interface
    std::string interface;
    if (!std::getline(linestream, interface, ',')) return 1;

    // Seventh cell is RT in milliseconds, integer
    if (!std::getline(linestream, cell, ',')) return 1;
    int16_t rt = (int16_t) std::stoll(cell);

    try {
      AddCall(traceid, timestamp, rpcid, um, rpctype, dm, interface, rt);
    } catch (const std::exception &e) {
      std::cout << "Got exception adding line: " << line << " " << e.what() << std::endl;
      return 100;
    }

    return 0;
  }

  /* 
  Load a file into the dataset.
  */
  int AddFile(std::string filename) {
    std::cout << "Processing " << filename << std::endl;
    std::map<int, int> statuscodes;

    // Open file and check headers
    std::ifstream file(filename);
    std::string expected_headers = ",traceid,timestamp,rpcid,um,rpctype,dm,interface,rt";
    std::string headers;
    std::getline(file, headers);

    if (headers != expected_headers) {
      std::cout << "  Header mismatch." << std::endl;
      std::cout << "  Expect: " << expected_headers << std::endl;
      std::cout << "  Found:  " << headers << std::endl;
      std::cout << "Skipping " << filename << std::endl;
      return 1;
    }

    // Process file
    std::string line;
    int linecount = 0;
    while(std::getline(file, line)) {
      int result = AddLine(line);
      statuscodes[result]++;
      linecount++;
    }
    std::cout << "  Processed " << linecount << " lines" << std::endl;
    int badlinecount = 0;
    for (auto &pair : statuscodes) {
      if (pair.first != 0) {
        std::cout << "    " << pair.second << " invalid lines (status " << pair.first << ")" << std::endl;
        badlinecount += pair.second;
      }
    }
    if (badlinecount == 0) {
      std::cout << "   All lines valid" << std::endl;
    }

    return 0;
  }

  Service* GetService(std::string &id) {
    auto it = services.find(id);
    if (it != services.end()) {
      return it->second;
    }
    Service* service = new Service(id, service_count++);
    services[id] = service;
    return service;
  }

  Interface* GetInterface(std::string &id) {
    auto it = interfaces.find(id);
    if (it != interfaces.end()) {
      return it->second;
    }
    Interface* interface = new Interface(id, interface_count++);
    interfaces[id] = interface;
    return interface;
  }

  Trace* GetTrace(std::string &id) {
    auto it = traces.find(id);
    if (it != traces.end()) {
      return it->second;
    }
    Trace* trace = new Trace(id, trace_count++);
    traces[id] = trace;
    return trace;
  }

  std::vector<Call*> calls;
  std::map<std::string, Service*> services;
  std::map<std::string, Interface*> interfaces;
  std::map<std::string, Trace*> traces;

  uint32_t call_count = 0;
  uint32_t span_count = 0;
  uint32_t service_count = 0;
  uint32_t interface_count = 0;
  uint32_t trace_count = 0;
};

inline void summarize(Dataset* d) {
  std::cout << d->services.size() << " services" << std::endl;
  std::cout << d->interfaces.size() << " interfaces" << std::endl;
  std::cout << d->traces.size() << " traces" << std::endl;
  std::cout << d->calls.size() << " calls" << std::endl;
  std::cout << ((float) d->calls.size() / (float) d->traces.size()) << " average calls/trace" << std::endl;

  for (auto &p1 : rpctypes) {
    int count = 0;
    int sum_um = 0;
    int max_um = 0;
    for (auto &p : d->services) {
      Service* service = p.second;
      if (service->Is(p1.second)) {
        count++;
        sum_um += service->ums.size();
        if (service->ums.size() > max_um) {
          max_um = service->ums.size();
        }
      }
    }
    std::cout << count << " " << p1.first << " services.  ";
    std::cout << "Average " << ((float) sum_um / (float) count) << " unique UMs.";
    std::cout << " (max " << max_um << ")" << std::endl;
  }

}

inline Dataset* process(std::vector<std::string> filenames) {
  Dataset* dataset = new Dataset();

  for (auto &filename : filenames) {
    dataset->AddFile(filename);
  }

  return dataset;
}

}

#endif // SRC_ALIBABA_H_