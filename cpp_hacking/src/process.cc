#include <iostream>
#include <argp.h>
#include "alibaba.h"
#include "mpi.h"
#include "convert.h"
#include "topology.h"
#include "jaegify.h"

const char *program_version = "process-alibaba-traces 1.0";
const char *program_bug_address = "<cld-science@mpi-sws.org>";
static char doc[] = "Processes Alibaba traces from https://github.com/alibaba/clusterdata/tree/master/cluster-trace-microservices-v2021";
static char args_doc[] = "CSVFILE";

static struct argp_option options[] = {
  // {"concurrency",  'c', "NUM",  0,  "Placeholder" },
  { 0 }
};

struct arguments {
  std::vector<std::string> csvfiles;
};

static error_t parse_opt (int key, char *arg, struct argp_state *state) {
  struct arguments *arguments = (struct arguments*) state->input;

  switch (key)
    {
    // case 'c':
    //   arguments->server_threads = atoi(arg);
    //   break;
    case ARGP_KEY_ARG:

      arguments->csvfiles.push_back(std::string(arg));
      break;

    case ARGP_KEY_END:
      if (state->arg_num < 1)
        /* Not enough arguments. */
        argp_usage (state);
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };

int main(int argc, char *argv[]) {
  struct arguments arguments;

  /* Default values. */
  arguments.csvfiles = {};

  /* Parse the arguments */
  argp_parse (&argp, argc, argv, 0, 0, &arguments);
  
  std::cout << "Processing:" << std::endl;
  for (int i = 0; i < arguments.csvfiles.size(); i++) {
    std::cout << " " << arguments.csvfiles[i] << std::endl;
  }

  alibaba::Dataset* alibabadataset = alibaba::process(arguments.csvfiles);
  alibaba::summarize(alibabadataset);

  mpi::Dataset* mpidataset = convert::ConvertDataset(alibabadataset);

  // topology::Generator generator(mpidataset);
  // generator.writeTopology("topology.json", 1);

  jaegify::Converter converter(mpidataset);
  converter.populateJaegerTraces();
  converter.writeTraces(false,"simple");

}
