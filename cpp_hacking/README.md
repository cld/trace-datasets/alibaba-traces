### Building

```
mkdir build
cd build
cmake ..
make -j
```

### Running
```
./process_traces ../data/example.csv
```

This will process the raw alibaba trace data contained in `../data/example.csv`.

By default, `process_traces` will load and parse all of the alibaba data, then calculate some statistics about services, then write a `topology.json` file which we're using for [hindsight-grpc](https://gitlab.mpi-sws.org/cld/tracing/hindsight-grpc).

### Using real trace data

In the example above, `../data/example.csv` is just a 1000-line snippet of the real trace data.

You have to get the real trace data from the [Alibaba repository](https://github.com/alibaba/clusterdata/tree/master/cluster-trace-microservices-v2021) or you can ask Vaastav which MPI machines already have the data.  Note: if you are on MPI machines, don't try to extract the trace data into your home directory, as this will fill up your NFS space.  Instead only put the trace data on the `/local` mount.

Once you have the data you can run, e.g.

```
./process_traces $ALIBABA_TRACE_DIRECTORY/MSCallGraph_0.csv
```

You can also provide multiple CSVs, which will take longer to process, but is necessary when using all the data, e.g.

```
./process_traces $ALIBABA_TRACE_DIRECTORY/MSCallGraph_0.csv $ALIBABA_TRACE_DIRECTORY/MSCallGraph_1.csv $ALIBABA_TRACE_DIRECTORY/MSCallGraph_2.csv
```

### Calculating different statistics

By default, `process_traces` calculates statistics about services and outputs a `topology.json` file which we're using for [hindsight-grpc](https://gitlab.mpi-sws.org/cld/tracing/hindsight-grpc).  If you are curious, [alibaba_topology.json](https://gitlab.mpi-sws.org/cld/tracing/hindsight-grpc/-/blob/main/config/alibaba_topology.json) in the `hindsight-grpc` repo contains the topology generated for trace files 1-14.

If you want something different, you can use this code as a starting point for calculating your own information from the trace dataset.

The workflow of the code is, roughly:

1. [src/alibaba.h](src/alibaba.h) has code for loading the raw trace data from file and turning it into in-memory objects representing the traces and calls
2. [src/convert.h](src/convert.h) has code for processing the loaded traces, removing duplicate calls, fixing up broken trace data, etc.
3. [src/mpi.h](src/mpi.h) has the sanitized/processed trace representation which will be more appropriate to work with than the raw trace data
4. [src/topology.h](src/topology.h) then has the code for taking an MPI trace representation and producing a `topology.json` file.

A good starting point is to look at the trace representation in [src/mpi.h](src/mpi.h), then implement logic similar to that found in [src/topology.h](src/topology.h) for your custom processing.


