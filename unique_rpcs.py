import pandas as pd
import os

def main():
    data_dir = "./data"
    call_graph_prefix = "MSCallGraph/MSCallGraph_"
    call_graph_dir = os.path.join(data_dir, call_graph_prefix)
    connections = {} # Map of um to set of dms
    df_final = pd.read_csv("rpcids_0_78.csv")
    for i in range(79, 110):
        print("Parsing file", i)
        file = call_graph_dir + str(i) + ".csv"
        df = pd.read_csv(file)
        df_final = df_final.append(df, ignore_index=True)
        df_final.drop_duplicates(subset=['um', 'dm', 'rpcid'],keep='last').reset_index(drop=True)
        del df
    rpcidfile = "./rpcids_0_78.csv"
    df_final.to_csv(rpcidfile,index=False,columns=['um','dm','rpcid'])

if __name__ == '__main__':
    main()
