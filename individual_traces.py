import pandas as pd
import os

class Trace:
    def __init__(self):
        self.calls = []

class Call:
    def __init__(self, traceid, timestamp, rpcid, um, dm, rpctype, interface, rt):
        self.traceid = traceid
        self.timestamp = timestamp
        self.rpcid = rpcid
        self.um = um
        self.dm = dm
        self.rpctype = rpctype
        self.interface = interface
        self.rt = rt

    def string(self):
        return self.traceid + "," + str(self.timestamp) + "," + self.rpcid + "," + self.um + "," + self.dm + "," + self.rpctype + "," + self.interface + "," + str(self.rt)

def main():
    data_dir = "./data"
    call_graph_prefix = "MSCallGraph/MSCallGraph_"
    call_graph_dir = os.path.join(data_dir, call_graph_prefix)
    out_dir = "/mnt/clusterdata/cluster-trace-microservices-v2021/data/traces"
    header_str = "traceid,timestamp,rpcid,um,dm,rpctype,interface,rt"
    for i in range(13, 145):
        out_dir_path = out_dir + str(i)
        os.mkdir(out_dir_path)
        print("Parsing file", i)
        file = call_graph_dir + str(i) + ".csv"
        df = pd.read_csv(file)
        traces = {}
        for index, row in df.iterrows():
            tid = str(row['traceid'])
            if tid not in traces:
                traces[tid] = Trace()
            trace = traces[tid]
            call = Call(str(row['traceid']), int(row['timestamp']), str(row['rpcid']), str(row['um']), str(row['dm']), str(row['rpctype']), str(row['interface']), int(row['rt']))
            trace.calls += [call]
            traces[tid] = trace
        del df
        count = 1
        for tid, trace in traces.items():
            if count % 10000 == 0:
                print("Storing trace", count)
            out_path = os.path.join(out_dir_path, tid + ".csv")
            if not os.path.exists(out_path):
                with open(out_path, 'w') as outf:
                    outf.write(header_str + "\n")
            with open(out_path, 'a') as outf:
                call_str = '\n'.join([call.string() for call in trace.calls])
                outf.write(call_str + "\n")
            count += 1
        del traces

if __name__ == '__main__':
    main()
