import sys
import pandas as pd

def process(arg1, arg2):
    trace_ids = set()
    for i in range(arg1, arg2+1):
        df = pd.read_csv('MSCallGraph_' + str(i) + '.csv')
        trace_gp = df.groupby('traceid')

        for key, item in trace_gp:
            new_trace_df = trace_gp.get_group(key)
            if key not in trace_ids:
                trace_path = '/local/clusterdata_output/' + str(key) + '.csv'
                trace_ids.add(key)
                new_trace_df.to_csv(trace_path)
            else:
                trace_path = '/local/clusterdata_output/' + str(key) + '.csv'
                new_trace_df.to_csv(trace_path, mode='a', index=False, header=False)

def main():
    if len(sys.argv) != 3:
        print("Usage: python indi_traces.py [start_file_num] [end_file_num]")
        sys.exit(1)
    arg1 = int(sys.argv[1])
    arg2 = int(sys.argv[2])
    process(arg1, arg2)

if __name__ == '__main__':
    main()
