import pandas as pd
from os import listdir, remove
from os.path import join
import shutil
import sys

def process(src_path, dst_path):
    src_files = [f for f in listdir(src_path)]
    dst_files = [f for f in listdir(dst_path)]

    duplicate_trace_files = set(dst_files).intersection(src_files)
    print(len(duplicate_trace_files))
    for file in duplicate_trace_files:
        trace_data = pd.read_csv(join(src_path, file))
        trace_data.to_csv(join(dst_path, file), mode='a', index=False, header=False)
        remove(join(src_path, file))

#    single_trac_files = set(src_files) - duplicate_trace_files
#    for file in single_trac_files:
#        shutil.move(join(src_path, file), join(dst_path, file))

def main():
    if len(sys.argv) != 3:
        print("Usage: python merge_traces.py [source_files_path] [destination_file_path]")
        sys.exit(1)
    arg1 = sys.argv[1]
    arg2 = sys.argv[2]
    process(arg1, arg2)

if __name__ == '__main__':
    main()
