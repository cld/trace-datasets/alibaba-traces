import pandas as pd
import os

def main():
    data_dir = "./data"
    call_graph_prefix = "MSCallGraph/MSCallGraph_"
    call_graph_dir = os.path.join(data_dir, call_graph_prefix)
    count_map = {}
    for i in range(145):
        file = call_graph_dir + str(i) + ".csv"
        df = pd.read_csv(file)
        value_counts = df['traceid'].value_counts()
        indices = value_counts.index
        for i in range(len(value_counts)):
            index = indices[i]
            val = value_counts[i]
            if index not in count_map:
                count_map[index] = val
            else:
                count_map[index] += val
        del df

    for k, v in count_map.items():
        print(str(k) + "," + str(v))

if __name__ == '__main__':
    main()
