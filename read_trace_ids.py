from os import listdir

def process():
    files_path = 'clusterdata_output'
    files = [f for f in listdir(files_path)]
    file_names = open('trace_file_names.txt', 'w')
    for f in files:
        file_names.write(f + "\n")
    file_names.close()


def main():
    process()


if __name__ == '__main__':
    main()
