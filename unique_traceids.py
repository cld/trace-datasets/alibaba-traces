import pandas as pd
import os

def main():
    data_dir = "./data"
    call_graph_prefix = "MSCallGraph/MSCallGraph_"
    call_graph_dir = os.path.join(data_dir, call_graph_prefix)
    trace_set = set()
    for i in range(145):
        print("Parsing file", i)
        file = call_graph_dir + str(i) + ".csv"
        df = pd.read_csv(file)
        unique_vals = df['traceid'].unique()
        for v in unique_vals:
            trace_set.add(v)
        del df
        print("Total Unique Traces:", len(trace_set))
    with open('unique_traces.csv', 'w') as outf:
        for tid in trace_set:
            outf.write(str(tid) + "\n")

if __name__ == '__main__':
    main()
