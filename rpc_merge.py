import pandas as pd
import os

def main():
    data_dir = "./data"
    call_graph_prefix = "MSCallGraph/MSCallGraph_"
    call_graph_dir = os.path.join(data_dir, call_graph_prefix)
    connections = {} # Map of um to set of dms
    df_final = pd.read_csv("rpcids_0_74.csv")
    df2 = pd.read_csv('rpcids_75_144.csv')
    df_final = pd.concat([df_final, df2]).drop_duplicates(subset=['um', 'dm', 'rpcid'], keep='last').reset_index(drop=True)
    del df2
    rpcidfile = "./rpcids.csv"
    df_final.to_csv(rpcidfile,index=False,columns=['um','dm','rpcid'])

if __name__ == '__main__':
    main()
