import pandas as pd
import os

def main():
    data_dir = "./data"
    call_graph_prefix = "MSCallGraph/MSCallGraph_"
    call_graph_dir = os.path.join(data_dir, call_graph_prefix)
    connections = {} # Map of um to set of dms
    unique_rpcs = set()
    for i in range(0, 74):
        print("Parsing file", i)
        file = call_graph_dir + str(i) + ".csv"
        df = pd.read_csv(file)
        for idx, row in df.iterrows():
            um = row['um']
            dm = row['dm']
            if um not in connections:
                connections[um] = set()
            if dm not in connections:
                connections[um].add(dm)
            rpcid = row['rpcid']
            if rpcid not in unique_rpcs:
                unique_rpcs.add(str(um) + "," + str(rpcid) + "," + str(dm))
        del df
    outfile = "./service_graph_0_74.txt"
    with open(outfile, 'w') as outf:
        for um, dm_list in connections.items():
            for dm in dm_list:
                outf.write(str(um) + " " + str(dm) + "\n")
    rpcidfile = "./rpcids_0_74.csv"
    with open(rpcidfile, 'w') as outf:
        outf.write("source,id,destination\n")
        for rpcid in unique_rpcs:
            outf.write(rpcid+"\n")

if __name__ == '__main__':
    main()
